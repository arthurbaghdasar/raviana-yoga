<?php
/**
 * @file
 * yoga_classes_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function yoga_classes_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-yoga-classes.
  $menus['menu-yoga-classes'] = array(
    'menu_name' => 'menu-yoga-classes',
    'title' => 'Yoga Classes',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Yoga Classes');


  return $menus;
}
