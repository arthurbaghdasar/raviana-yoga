<?php

function mnmedia_commerce_commerce_form($form, &$form_state) {
	$form = array();
	$form['mnmedia_commerce'] = array(
		'#type' => 'vertical_tabs',
	);

	$form['mnmedia_commerce']['album'] = array(
		'#type' => 'fieldset',
		'#title' => t('Albums'),
		'#group' => 'basic',
	);
	
	$form['mnmedia_commerce']['album']['album_product_type'] = array(
		'#type' => 'select',
		'#title' => t('Product Variant'),
		'#options' => commerce_product_type_options_list(),
		'#description' => t('Choose Commerce Product Variant Album will save into.'),
		'#default_value' => variable_get('album_product_type', ''),
	);

	
	$form['mnmedia_commerce']['album']['album_product_node_type'] = array(
		'#type' => 'select',
		'#title' => t('Product Display (node type)'),
		'#options' => mnmedia_commerce_get_product_display_types(),
		'#description' => t('Choose Commerce Product Display Album will save into.'),
		'#default_value' => variable_get('album_product_node_type', ''),
	);

	$form['mnmedia_commerce']['track'] = array(
		'#type' => 'fieldset',
		'#title' => t('Tracks'),
		'#group' => 'basic',
	);
	
	$form['mnmedia_commerce']['track']['track_product_type'] = array(
		'#type' => 'select',
		'#title' => t('Product Variant'),
		'#options' => commerce_product_type_options_list(),
		'#description' => t('Choose Commerce Product Variant Track will save into.'),
		'#default_value' => variable_get('track_product_type', ''),
	);
	
	$form['mnmedia_commerce']['track']['track_product_node_type'] = array(
		'#type' => 'select',
		'#title' => t('Product Display (node type)'),
		'#options' => mnmedia_commerce_get_product_display_types(),
		'#description' => t('Choose Commerce Product Display Track will save into.'),
		'#default_value' => variable_get('track_product_node_type', ''),
	);
	return system_settings_form($form);
}

function mnmedia_commerce_get_product_display_types() {
	$node_types = array('' => 'choose...');
	foreach (node_type_get_types() as $id => $node_type) {
		$node_types[$id] = $node_type->name;
	}
	return $node_types;
}