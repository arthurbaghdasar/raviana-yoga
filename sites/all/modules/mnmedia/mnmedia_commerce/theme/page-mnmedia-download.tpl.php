<?php if(!empty($locations)):?>
	<h2 class="title">Download Locations:</h2>
	<ul>
		<?php foreach ($locations as $location):?>
			<?php $track = mnmedia_getTrackInfo($location->MnetId);?>
			<li><?php print $track;?>: <a href="<?php print $location->Location;?>" title="Download Track" target="_blank">Download</a></li>
		<?php endforeach;?>
	</ul>
<?php endif;?>