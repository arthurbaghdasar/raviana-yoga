<?php
/**
 * @file
 * mnmedia_commerce.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function mnmedia_commerce_commerce_product_default_types() {
  $items = array();
  $items['product_mnmedia_album'] = array(
    'type' => 'product_mnmedia_album',
    'name' => 'MN Media Album',
    'description' => 'A product type of MN Media Album.',
    'help' => '',
    'revision' => '1',
  );
  $items['product_mnmedia_track'] = array(
    'type' => 'product_mnmedia_track',
    'name' => 'MN Media Track',
    'description' => 'A product type of MN Media Track.',
    'help' => '',
    'revision' => '1',
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function mnmedia_commerce_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function mnmedia_commerce_node_info() {
  $items = array();
  $items['product_mnmedia_album_display'] = array(
    'name' => t('MN Media Album Product'),
    'base' => 'node_content',
    'description' => 'Use <em>MN Media Album Product</em> for adding Albums from the local catalog',
    'has_title' => '1',
    'title_label' => t('Album Title'),
    'help' => '',
  );
  $items['product_mnmedia_track_display'] = array(
    'name' => t('MN Media Track Product'),
    'base' => 'node_content',
    'description' => 'Use <em>MN Media Track Product</em> for adding Tracks from the local catalog',
    'has_title' => '1',
    'title_label' => t('Track Title'),
    'help' => '',
  );
  return $items;
}


/**
 * Implements hook_node_type_insert().
 */
function mnmedia_commerce_node_type_insert($content_type) {
  if ($content_type->type == 'product_mnmedia_album_display') {

    // Create all the fields we are adding to our content type.
    foreach (_mnmedia_commerce_installed_fields() as $field) {
      field_create_field($field);
    }

    // Create all the instances for our fields.
    foreach (_mnmedia_commerce_installed_instances() as $instance) {
      $instance['entity_type'] = 'node';
      $instance['bundle'] = 'product_mnmedia_album_display';
      field_create_instance($instance);
    }
  }
}


function _mnmedia_commerce_installed_fields() {
  return array(
    'mnmedia_album_artist' => array(
      'field_name' => 'mnmedia_album_artist',
      'cardinality' => 3,
      'type' => 'taxonomy_term_reference',
      'instance_settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => 'mnmedia_artist',
            'parent' => 0
          ),
        ),
      ),
    ),
  );
}


function _mnmedia_commerce_installed_instances() {
  return array(
    'mnmedia_album_artist' => array(
      'field_name' => 'mnmedia_album_artist',
      'label' => t('The artist of this Album.'),
      'widget' => array(
        'type' => 'options_select',
      ),
      'display' => array(
        'default' => array('type' => 'hidden'),
        'teaser' => array('type' => 'hidden'),
      ),
    ),
  );
}