<?php

/**
 * Implements hook_schema().
 */
function mnmedia_schema() {
	$schema['mnmedia_albums'] = array(
	    'description' => "Stores MN Media Albums.",
	    'fields' => array(
			'mnetid' => array(
				'description' => "MnetId for each {mnmedia} Album",
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'artist_id' => array(
				'description' => "MnetId for Album Artist",
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => FALSE,
				'default' => 0,
			),
			'title' => array(
				'description' => 'Album Title',
				'type' => 'varchar',
				'length' => 255,
				'not null' => TRUE,
				'default' => '',
			),
			'genre' => array(
				'description' => 'Album Genre',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'label' => array(
				'description' => 'Album Label',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'duration' => array(
				'description' => 'Duration of Album',
				'type' => 'varchar',
				'length' => 10,
				'not null' => FALSE,
				'default' => '',
			),
			'album_art_75' => array(
				'description' => 'Album Art 75x75',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'album_art_150' => array(
				'description' => 'Album Art 150x150',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'album_art_800' => array(
				'description' => 'Album Art 800x800',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'price' => array(
				'description' => 'Album Price',
				'type' => 'float',
				'length' => 'tiny',
				'not null' => FALSE,
				'default' => 0,
			),
			'currency' => array(
				'description' => 'Album Currency',
				'type' => 'varchar',
				'length' => 3,
				'not null' => FALSE,
				'default' => '',
			),

		),
	    'indexes' => array(
	    	'title' => array('title'),
	    	'artist_id' => array('artist_id')
	    ),
	    'primary key' => array('mnetid'),
  	);
	$schema['mnmedia_tracks'] = array(
	    'description' => "Stores MN Media Tracks.",
	    'fields' => array(
			'mnetid' => array(
				'description' => "MnetId for each {mnmedia} Track",
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'artist_id' => array(
				'description' => "MnetId for Artist",
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => FALSE,
				'default' => 0,
			),
			'album_id' => array(
				'description' => "MnetId for Album",
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => FALSE,
				'default' => 0,
			),
			'title' => array(
				'description' => 'Track Title',
				'type' => 'varchar',
				'length' => 255,
				'not null' => TRUE,
				'default' => '',
			),
			'genre' => array(
				'description' => 'Track Genre',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'duration' => array(
				'description' => 'Duration of Track',
				'type' => 'varchar',
				'length' => 10,
				'not null' => FALSE,
				'default' => '',
			),
			'track_number' => array(
				'description' => 'Track Number',
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => FALSE,
				'default' => 0,
			),
			'can_sample' => array(
				'description' => 'Can Sample',
				'type' => 'int',
				'not null' => TRUE,
				'default' => 0,
			),
			'sample_location' => array(
				'description' => 'Sample Location',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'sample_resource' => array(
				'description' => 'Sample Resource',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'price' => array(
				'description' => 'Track Price',
				'type' => 'float',
				'length' => 'tiny',
				'not null' => FALSE,
				'default' => 0,
			),
			'currency' => array(
				'description' => 'Track Currency',
				'type' => 'varchar',
				'length' => 3,
				'not null' => FALSE,
				'default' => '',
			),
		),
	    'indexes' => array(
	    	'title' => array('title'),
	    	'artist_id' => array('artist_id'),
	    	'album_id' => array('album_id')
	    ),
	    'primary key' => array('mnetid'),
  	);

	$schema['mnmedia_artists'] = array(
	    'description' => "Stores MN Media Artists.",
	    'fields' => array(
			'mnetid' => array(
				'description' => "MnetId for each {mnmedia} Album",
				'type' => 'int',
				'unsigned' => TRUE,
				'not null' => TRUE,
				'default' => 0,
			),
			'name' => array(
				'description' => 'Artist Name',
				'type' => 'varchar',
				'length' => 255,
				'not null' => TRUE,
				'default' => '',
			),
			'artist_image_180' => array(
				'description' => 'Artist Image 180x80',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'artist_image_190' => array(
				'description' => 'Artist Image 190x230',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
			'artist_image_375' => array(
				'description' => 'Artist Image 375x250',
				'type' => 'varchar',
				'length' => 255,
				'not null' => FALSE,
				'default' => '',
			),
		),
	    'indexes' => array(
	    	'name' => array('name'),
	    ),
	    'primary key' => array('mnetid'),
  	);
	return $schema;
}

/**
 * Implements hook_field_schema().
 */
function mnmedia_field_schema($field) {
  $columns = array(
    'album_id' => array('type' => 'varchar', 'length' => 20, 'not null' => FALSE),
  );
  $indexes = array(
    'album_id' => array('album_id'),
  );
  return array(
    'columns' => $columns,
    'indexes' => $indexes,
  );
}

/**
 * Implements hook_uninstall().
 */
function mnmedia_uninstall() {
  // Remove variables
  // variable_del('mnmedia_environment');
  // variable_del('include_explicit');
  // variable_del('mn_production_api_key');
  // variable_del('mn_production_shared_secret');
  // variable_del('mn_integration_api_key');
  // variable_del('mn_integration_shared_secret');
}