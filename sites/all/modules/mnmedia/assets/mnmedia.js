(function ($) {
    Drupal.behaviors.mnmedia_sample =  {
        attach: function(context, settings) {
            $(document).ready(function () {
                var intervalId;
                var dataPlayer = null;
                var playButton = $('.play_pause');
                var supportsFlash = true;
                // if (flashembed.isSupported([10, 1])) {
                //     supportsFlash = true;
                // }

                playButton.click(function() {
                    $(this).toggleClass('active');
                    var mnetid = $(this).attr('data-mnetid');
                    if(!$(this).hasClass('active')) {
                        if (supportsFlash){
                            $f('player-' + mnetid).pause();
                        } else {
                            $('html5Audio-' + mnetid).pause();
                        }
                    } else if($f('player-' + mnetid) && $f('player-' + mnetid).isPaused()) {
                        if (supportsFlash){
                            $f('player-' + mnetid).resume();
                        } else {
                            $('html5Audio-' + mnetid).play();
                        }
                        
                    } else if($(this).attr('data-mp3')) {
                        if (supportsFlash) {
                            $f('player-' + mnetid, "/sites/all/modules/mnmedia/assets/flowplayer-3.2.18.swf", {
                                clip: {
                                    url: $(this).attr('data-location'),
                                    provider: "audio"
                                },
                                plugins: {
                                    audio: {
                                        url: "/sites/all/modules/mnmedia/assets/flowplayer.audio-3.2.11.swf",
                                    },
                                    controls: null,
                                }
                            }).ipad();
                        } else {
                            var html5Audio = $('<audio />', {
                                autoPlay : 'autoplay',
                                controls : 'controls',
                                id: 'html5Audio-' + mnetid,
                            });
                            $('<source />', {type: 'audio/mpeg; codecs="mp3"', 'src': $(this).attr('data-location') }).appendTo(html5Audio);
                            html5Audio.appendTo('#player-'+ mnetid);
                        }
                    } else {
                        if (supportsFlash) {
                            $f('player-' + mnetid, "/sites/all/modules/mnmedia/assets/flowplayer-3.2.18.swf", {
         
                                clip: {
                                    url: $(this).attr('data-resource'),
                                    scaling: 'fit',
                                    // configure clip to use hddn as our provider, referring to our rtmp plugin
                                    provider: 'hddn',
                                    onNetStreamEvent: function(param1, eventype, eventinfo) {
                                        console.log(param1);
                                        console.log(eventype);
                                        console.log(eventinfo);
                                    },
                                    onUpdate: function() {
                                        
                                    },
                                },
                             
                                // streaming plugins are configured under the plugins node
                                plugins: {
                             
                                    // here is our rtmp plugin configuration
                                    hddn: {
                                        url: "/sites/all/modules/mnmedia/assets/flowplayer.rtmp-3.2.13.swf",
                             
                                        // netConnectionUrl defines where the streams are found
                                        netConnectionUrl: $(this).attr('data-location')
                                    },
                                    controls: null,
                                },
                                play: null,
                                canvas: {
                                    backgroundGradient: 'none'
                                },
                                debug: true,
                                simulateiDevice: true,
                                // log : {
                                //     level: 'debug',
                                // },
                            }).ipad();
                        } else {
                            var notCompatibleError = $('<p />', {
                                html: 'Your device does not support this stream.'
                            });
                            notCompatibleError.appendTo('#player-'+ mnetid);
                        }
                    }
                    
                });
            });
        }
    };
})(jQuery);