<?php
/**
 * @file
 * ltv_track_importer.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function ltv_track_importer_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'mp3_importer';
  $feeds_importer->config = array(
    'name' => 'LTV track importer',
    'description' => 'importer for single mp3s',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          0 => 'public',
          1 => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsCommerceProductProcessor',
      'config' => array(
        'product_type' => 'ltv_track_ii',
        'author' => '129',
        'tax_rate' => TRUE,
        'mappings' => array(
          0 => array(
            'source' => 'SKU',
            'target' => 'sku',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Track sample',
            'target' => 'field_track_sample_rev',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Commerce file',
            'target' => 'commerce_file',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Price',
            'target' => 'commerce_price:amount',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Track thumb',
            'target' => 'field_track_thumb',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'License duration',
            'target' => 'commerce_license_duration',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Album ref',
            'target' => 'field_album_reference:etid',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
        'bundle' => 'ltv_track_ii',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '1800',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['mp3_importer'] = $feeds_importer;

  return $export;
}
