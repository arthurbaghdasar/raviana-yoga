<?php
/**
 * @file
 * ltv_album_display_importer_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ltv_album_display_importer_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "feeds_tamper" && $api == "feeds_tamper_default") {
    return array("version" => "2");
  }
}
