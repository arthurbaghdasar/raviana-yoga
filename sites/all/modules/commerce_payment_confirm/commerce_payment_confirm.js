/**
 * @file
 * Alters the "pay now" link and adds the confirm dialog.
 */

(function ($) {
  Drupal.behaviors.twogbshopConfirm = {
    attach: function(context, settings) { 
      var payNow = Drupal.t('Pay Now');
      var newcheckout = $('<a href="#" class="pay-now-confirm">' + payNow + '</a>');
      $('input.checkout-continue:not(.checkout-confirm-processed)', context)
      .addClass('checkout-confirm-processed')
      .addClass('element-invisible').attr('disabled','true');
      newcheckout.insertAfter('.checkout-confirm-processed');
      newcheckout.click(function() {
        $( "#commerce-confirm-dialog" ).dialog({
          resizable: false,
          height:300,
          width:500,
          modal: true,
          buttons: {
            "Confirm": function() {
              $( this ).dialog( "close" );
              $('.pay-now-confirm').remove();
              $('.checkout-confirm-processed').removeClass('element-invisible')
              .removeAttr('disabled')
              .click();
              return true;
            },
            Cancel: function() {
              $( this ).dialog( "close" );
            }
          }
        });
        return false;
        
      });
    }
  }
})(jQuery);
