<?php

// Plugin definition
$plugin = array(
  'title' => t('Yoga Landing Two column stacked'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol_yoga_landing.png',
  'theme' => 'panels_twocol_yoga_landing',
  'css' => 'twocol_yoga_landing.css',
  'regions' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
