<?php
/**
 * @file
 * yoga_landing_page.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function yoga_landing_page_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'yoga_landing_page';
  $page->task = 'page';
  $page->admin_title = 'Yoga landing page';
  $page->admin_description = 'This is the current yoga streams landing page';
  $page->path = 'yoga-landing';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_yoga_landing_page__panel_context_321c284f-000a-4112-8d0d-c6fdb6a1387b';
  $handler->task = 'page';
  $handler->subtask = 'yoga_landing_page';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Yoga landing page',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Yoga Streams';
  $display->uuid = 'af081ae8-c447-45b8-96f0-321d6a631427';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-c5931f21-19da-4964-82a8-46fdc0f981f7';
    $pane->panel = 'center';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => '',
      'title' => '',
      'body' => '<img src="/sites/default/files/Offering.jpg" alt="Yoga Streams"/>
<p class="yoga-landing">Now you can do your practice any time / anywhere on any device! Practice makes perfect and when you get down on your mat and rock your discipline you are successful as any being can be because you have expressed the willingness to go up against inertia and relate to Infinity from the finite. Rather than offer access to these classes for a limited time, you will receive LIFETIME access to these programs when you make a purchase! We wish you beautiful breakthroughs and blessings on all levels!</p>
',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'c5931f21-19da-4964-82a8-46fdc0f981f7';
    $display->content['new-c5931f21-19da-4964-82a8-46fdc0f981f7'] = $pane;
    $display->panels['center'][0] = 'new-c5931f21-19da-4964-82a8-46fdc0f981f7';
    $pane = new stdClass();
    $pane->pid = 'new-bf8717d6-3048-4930-8b44-adcd1eebdcc9';
    $pane->panel = 'center';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '1988',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'teaser',
      'link_node_title' => 0,
      'override_title' => 1,
      'override_title_text' => '<none>',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'bf8717d6-3048-4930-8b44-adcd1eebdcc9';
    $display->content['new-bf8717d6-3048-4930-8b44-adcd1eebdcc9'] = $pane;
    $display->panels['center'][1] = 'new-bf8717d6-3048-4930-8b44-adcd1eebdcc9';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-c5931f21-19da-4964-82a8-46fdc0f981f7';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['yoga_landing_page'] = $page;

  return $pages;

}
