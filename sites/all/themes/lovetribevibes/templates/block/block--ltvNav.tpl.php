<?php

/**
 * @file
 * Minimal theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: An ID for the block, unique within each module.
 * - $block->region: The block region embedding the current block.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - block: The current template type, i.e., "theming hook".
 *   - block-[module]: The module generating the block. For example, the user
 *     module is responsible for handling the default user navigation block. In 
 *     that case the class would be 'block-user'.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 * - $block_html_id: A valid HTML ID and guaranteed unique.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php
  $search_new_block = module_invoke('views', 'block_view', '-exp-search_products-page_search_products');
  
?>
 



<nav<?php print $attributes; ?>>
<div id="top-nav" class="top-nav">
		<ul>
			<?php if(user_is_logged_in()) { ?>
				<li><a href="/user">My Account</a></li><li><a href="/user/logout">Log Out</a></li>
				<?php } else { ?>
				<li><a href="/user/login">Log In</a></li>
				<li><a href="/user/register">Create Account</a></li>
				<?php	} ?>
		<li><a href="/contact">Contact</a></li>
		</ul>
</div>
<div id="main-nav-wrapper" class="main-nav__wrapper"><!-- main-nav__wrapper -->
	<div id="main-nav-inner-wrapper" class="main-nav__inner-wrapper"><!-- main-nav__inner-wrapper -->
		<ul id="ltv-main-nav" class="main-nav"><!-- main-nav -->
			<li class="home-logo main-nav__li--home"><!--main-nav__li--home --><a href="/" class="main-nav__home-link"><span class="main-nav__site-name">RAVIANA <span class="main-nav__site-name--streams site-name--streams">STREAMS</span></span></a></li>
			<li class="home-icon main-nav__li--home-icon"><!-- main-nav__li--home-icon --><a href="/" class="main-nav__home-icon-link"><img src="/sites/default/files/home-icon-trans.png" height="22" alt="home icon" class="main-nav__home-icon"></a></li>
			<li class="main-nav__li--mission main-nav__txt"><a href="/mission">Mission</a></li>
			<!-- <li class="shop-music sep txt"><a href="/browse-albums">Music</a></li> -->
			<li class="shop-dvds main-nav__txt main-nav__li--yoga-streams"><a href="/yoga">Yoga Streams</a></li>
			<li class="main-nav__txt no-anchor dropdown main-nav__li--shop"><span>Shop</span>
				<ul class="sub-menu main-nav__sub-menu">
					<li class="main-nav__sub-li--dvds"><a href="/dvds">DVD's</a></li>
					<li class="main-nav__sub-li--books"><a href="https://www.raviana.com/books/kundalini-yoga-book" target="_blank">Books</a></li>
				</ul>
			</li> 
			
			<li span class="cart main-nav__li--cart"><a href="/cart" class="flaticon-shopping-cart13 main-nav__cart-icon"></a></li> 

			<?php  $ajaxCart = module_invoke('views', 'block_view', 'shopping_cart-block');?>
			<li id="ajax-shopping-cart-wrap" class=""><?php  $ajaxCart = module_invoke('views', 'block_view', 'shopping_cart-block');print render($ajaxCart['content'])?></li>	
			<li id="search-open" class="flaticon-magnifier13 search-open"></li>
		</ul> 
		
	<div id="search-container" class="search search-container">
		<?php print render($search_new_block['content']); ?>
	</div> 
	</div>
</div>
<div id="mobile-nav-wrapper" class="mobile-nav__wrapper">
	<ul id="mobile-header" class="mobile-header">
		<!-- <li class="home-logo"><a href="/" class="home-mobile-link"><img src="/sites/default/files/menu_logo_green.png"></a></li> -->
		<li class="mobile-header__home-logo"><a href="/" class="mobile-nav__home-link"><span class="mobile-nav__site-name">RAVIANNA <span class="mobile-nav__text-streams">STREAMS</span></span></a></li>
		<li class="mobile-header__icon-menu icon-menu"></li>
	</ul>
</div>
<ul id="mobile-nav" class="mobile-nav">
	<?php if(user_is_logged_in()) { ?>
		<li class="mobile-nav__li mobile-nav__li--logout"><a href="/user/logout" class="flaticon2-log-out">Logout</a></li>
		<li class="mobile-nav__li mobile-nav__li--my-account"><a href="/user" class="flaticon2-settings-1">My Account</a></li>
	<?php } else { ?>
		<li class="mobile-nav__li mobile-nav__li--login"><a href="/user/login" class="flaticon2-login-square-arrow-button-outline">Login</a></li>
		<li class="mobile-nav__li mobile-nav__li--create" ><a href="/user/register" class="flaticon2-plus">Create Account</a></li>
	<?php } ?>
	<li class="mobile-nav__li"><a href="/mission" class="flaticon2-interface-1">Mission</a></li>
	<li class="mobile-nav__li"><a href="/dvds" class="flaticon2-compact-disc">DVDs</a></li>
	<li class="mobile-nav__li"><a href="/yoga" class="flaticon2-meditation-yoga-posture">Yoga Classes</a></li> 
	<li class="mobile-nav__li"><a href="/books/lets-love-and-be-real" class="flaticon2-science-book">Books</a></li> 
	<li class="mobile-nav__li"><a href="/cart" class="flaticon-shopping-cart13">Cart</a></li>
	<li class="mobile-nav__li"><a href="/contact" class="flaticon2-message-in-a-speech-bubble">Contact</a></li>
	<li class="mobile-nav__li"><a href="/search-products" class="flaticon-magnifier13"> </a></li>
	
</ul>
</nav> 