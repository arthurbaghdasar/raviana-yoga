<?php
/*
**
 * Used to remove certain elements from the $head output within html.tpl.php
 *
 * @see http://api.drupal.org/api/drupal/modules--system--system.api.php/function/hook_html_head_alter/7
 * @param array $head_elements
 */
function lovetribevibes_html_head_alter(&$head_elements) {
    
    $remove = array(
        'system_meta_generator',
    );

    foreach ($remove as $key) {
        if (isset($head_elements[$key])) {
            unset($head_elements[$key]);
        }
    }

    // Use this loop to find out which keys are available.
    /* -- Delete this line to execute this loop
    echo '<pre>';
    foreach ($head_elements as $key => $element) {
        echo $key ."\n";
    }
    echo '</pre>';
    // */
}


function lovetribevibes_preprocess_html(&$vars) {
	//var_dump($vars['head']);
	//echo '<script>alert("helloz")</script>';
	}

function lovetribevibes_preprocess_page (&$vars) {
	//dpm($vars);
	}  

function lovetribevibes_preprocess_region(&$vars) {

	}

 
function lovetribevibes_form_alter(&$form, &$form_state, $form_id){
	if($form_id == 'search_block_form'){
		$form['search_block_form']['#attributes']['placeholder'] = t('enter search terms...');
		}
	//kpr($form);
	

	}

function lovetribevibes_preprocess_block(&$variables) {
  	if($variables['block_html_id'] == 'block-system-main-menu') {
		
		$variables['theme_hook_suggestions'][] = 'block__ltvNav';
		} 
	$search_block_form = drupal_get_form('search_block_form');
	$search_box = drupal_render($search_block_form);
	$variables['search_box'] = $search_box; 
	}
	
function lovetribevibes_preprocess_taxonomy_term(&$variables){
		//kpr($variables);
	}

function lovetribevibes_preprocess_node(&$vars){
	if($vars['type']=='ltv_album_ii') {
		drupal_add_js(path_to_theme().'/js/ltvalbum.js');
	}
	if($vars['type']=='video_class') {
		drupal_add_js('//content.jwplatform.com/libraries/RCQ3xz5Y.js');
		//drupal_add_js('https://content.jwplatform.com/libraries/BO3sb2Si.js');
	}
}

function lovetribevibes_commerce_checkout_complete($order) {

}

function lovetribevibes_menu_local_tasks_alter(&$vars){
	$count = $vars['tabs'][0]['count'];
	
	//dsm($vars);
	$vars['tabs'][0]['output'][0]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-eye-of-a-human';
	$vars['tabs'][0]['output'][1]['#link']['title'] = 'Settings';
	for($i=0;$i<$count;$i++) {
		if($vars['tabs'][0]['output'][$i]['#link']['title']=='Settings') {
			//echo '<script>alert("FOUND")</script>';
			$vars['tabs'][0]['output'][$i]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-settings-1';
		}
		if($vars['tabs'][0]['output'][$i]['#link']['title']=='Files') {
			//echo '<script>alert("FOUND")</script>';
			$vars['tabs'][0]['output'][$i]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-mp3';
		}
		if($vars['tabs'][0]['output'][$i]['#link']['title']=='Stored cards') {
			//echo '<script>alert("FOUND")</script>';
			$vars['tabs'][0]['output'][$i]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-credit-card';
		}
		if($vars['tabs'][0]['output'][$i]['#link']['title']=='Video Classes') {
			//echo '<script>alert("FOUND")</script>';
			$vars['tabs'][0]['output'][$i]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-meditation-yoga-posture';
		}
		if($vars['tabs'][0]['output'][$i]['#link']['title']=='Order history') {
			$vars['tabs'][0]['output'][$i]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-history';
		}
		if($vars['tabs'][0]['output'][$i]['#link']['title']=='Contact') {
			$vars['tabs'][0]['output'][$i]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-message-in-a-speech-bubble';
		}
		if($vars['tabs'][0]['output'][$i]['#link']['title']=='Address Book') {
			$vars['tabs'][0]['output'][$i]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-notebook-1';
		}
	} 

	
	//$vars['tabs'][0]['output'][1]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-settings-1';
	//$vars['tabs'][0]['output'][8]['#link']['localized_options']['attributes']['class'][] = 'flaticon2-meditation-yoga-posture';
	
}

function lovetribevibes_menu_alter(&$vars){
	//dsm($vars);
}

function lovetribevibes_form_views_exposed_form_alter(&$form, &$form_state, $form_id){
	
	if($form['#id'] == 'views-exposed-form-Latest-page') {
		$form['submit']['#value'] = t('Search');
		//kpr($form);
	}
}


