<div class="mp">
    <div class="mp__message-container">
        <h1 class="mp__title">Attention Yogis and Friends</h1>
       <span class="mp__message">Please note that within the next two weeks your raviana streams will be moving from LoveTribevibes.com to: <a href="https://www.raviana-yogastreams.com" class="mp__link">www.raviana-yogastreams</a>. Please set your bookmarks accordingly. And of course you can always access our streams from the home page of raviana.com
You will be receiving a e-mail notification as well. We wish you many wonderful workouts! </span>
        <span class="close-button">&times;</span>
    </div>
</div>
<div class="l-page">
  <header class="l-header" role="banner">
    

    <?php print render($page['header']); ?>
    <?php print render($page['topnav']); ?>
    <?php print render($page['navigation']); ?>
  </header>

  <div class="l-main">
    <?php
    /*
      $socialMediaBlock = block_load('block', '2');
      $output = drupal_render(_block_get_renderable_array(_block_render_blocks(array($socialMediaBlock))));
      print $output;
      */
    ?>
    <div class="l-content" role="main">
      <?php print render($page['highlighted']); ?>
      <!-- <?php print $breadcrumb; ?> -->
      
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php //if($is_admin): ?>
      
      <?php print $messages; ?>
      <?php //endif; ?>
      <?php print render($tabs); ?>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>

    <?php print render($page['sidebar_first']); ?>
    <?php print render($page['sidebar_second']); ?>
    
  </div><!-- end main -->
  <?php print render($page['super_footer']); ?>
  <div class="push"></div>
</div>

<footer>
<div class="l-footer" role="contentinfo">
    <?php print render($page['footer']); ?>
</div> 
</footer>


<script src="https://yogastreams.com/profiles/commerce_kickstart/modules/contrib/service_links/js/twitter_button.js?nx1rtc"></script>
<script src="https://yogastreams.com/profiles/commerce_kickstart/modules/contrib/service_links/js/facebook_like.js?nx1rtc"></script>
<script src="/sites/all/themes/yogastreams/js/twitter.js"></script>
<script>
    
    var modal = document.querySelector(".mp");
    var closeButton = document.querySelector(".close-button");

    function toggleModal() {
        modal.classList.toggle("show-modal");
    }

    function windowOnClick(event) {
        if (event.target === modal) {
            toggleModal();
        }
    }

    window.onload = toggleModal;
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);

    


</script>




