name = Homepage

description = Use this for homepage with the footer promo thing.
preview = preview.png
template = homepage-layout

; Regions
regions[branding]       = Branding
regions[header]         = Header
regions[content]        = Content
regions[footer]         = Footer

; Stylesheets
stylesheets[all][] = css/layouts/homepage/homepage.layout.css
stylesheets[all][] = css/layouts/homepage/homepage.layout.no-query.css
