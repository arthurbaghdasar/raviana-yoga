var API_URL = "http://cdn.api.twitter.com/1.1/urls/count.json",
    TWEET_URL = "https://twitter.com/intent/tweet";
    FACEBOOK_URL = "https://www.facebook.com/sharer/sharer.php?u="
     
jQuery(".tweet").click(function() {
    var elem = jQuery(this),
    // Use current page URL as default link
    url = encodeURIComponent(elem.attr("data-url") || document.location.href);
    // Use page title as default tweet message
    text = elem.attr("data-text") || document.title;
    via = elem.attr("data-via") || "";
    related = encodeURIComponent(elem.attr("data-related")) || "";
    hashtags = encodeURIComponent(elem.attr("data-hashtags")) || "";
    
    if (hashtags == "undefined"){
    	var tweet_url_string = TWEET_URL + "?url=" + url + "&via=" + via;
    }else{
    	var tweet_url_string = TWEET_URL + "?hashtags=" + hashtags + "&url=" + url + "&via=" + via;
    	}
    /*
    removed from URL
     "&original_referer=" +
                encodeURIComponent(document.location.href) + "&related=" + related +
                "&source=tweetbutton&text=" + text +
                */
    // Set href to tweet page
    elem.attr({
        href: tweet_url_string
    });
         
    // Get count and set it as the inner HTML of .count
    jQuery.getJSON(API_URL + "?callback=?&url=" + url, function(data) {
        elem.find(".count").html(data.count);
    });
    
});

jQuery(".fbshare").click(function () {   
		var elem = jQuery(this);
		url = encodeURIComponent(elem.attr("data-url") || document.location.href);	            
		window.open(FACEBOOK_URL + url, "PopupWindow", "width=500,height=500,scrollbars=yes,resizable=yes");
});