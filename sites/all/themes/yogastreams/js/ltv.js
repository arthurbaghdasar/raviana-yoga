jQuery(document).ready(function() {
/*MAIN SEARCH OPEN & CLOSE *********************************************************************/
  var searchContainerTimeout;

  function slideSearchUp(){
    jQuery('#search-container').slideUp('fast');  
  };

  jQuery('#search-open').mouseenter(
    function(){
      jQuery('#search-container').slideDown('fast');
      jQuery( "#search-container").removeClass( "closed" ).addClass( "open" );
      searchopen = true;
      console.log(searchopen);
  });
  jQuery('#searchopen, #search-container').mouseleave(function(){
    searchContainerTimeout = setTimeout(slideSearchUp, 2000);
    searchopen = false;
    console.log(searchclose);
  });
  jQuery('#search-container').mouseenter(function(){
    clearTimeout(searchContainerTimeout);
  //  searchContainerTimeout = setTimeout(slideSearchUp, 3000);
  });

  /* MAIN NAV SHOP DROPDOWN *****************************************************************/
  jQuery( '.dropdown' ).hover(
        function(){
          //alert('hover');
            jQuery(this).children('.sub-menu').css('opacity', 0);
            jQuery(this).children('.sub-menu').fadeIn(200);
            jQuery(this).children('.sub-menu').slideDown(200);
            jQuery(this).children('.sub-menu').animate({opacity:1}, {queue:false, duration:300}); 
        },
        function(){
            jQuery(this).children('.sub-menu').slideUp(200);
        }
  );
/* MOBILE */  
  var mobileNavOn = false;
  function MNSlideDown(){
    jQuery('#mobile-nav').slideDown();
    mobileNavOn = true;
  }
  function MNSlideUp(){
    jQuery('#mobile-nav').slideUp();
    mobileNavOn = false;
  }
  jQuery('.icon-menu').live("click", function(e){
    e.preventDefault();
    MNSlideDown();
  });
	
  jQuery('#mobile-nav li').live("click", function(){
    MNSlideUp();
  });

  jQuery('#mobile-nav').mouseleave(function(){
    MNSlideUp()
  });
  
  jQuery('.join-us-homepage-footer').focus( function(){
        jQuery(this).val(''); 
        jQuery(this).attr("placeholder", "");
        return;
  })

}); 